provider "aws" {
}

variable "cidr_blocks" {
    description = "subnet cidr block for vpc and subnet"
    type = list(string)

}


resource "aws_vpc" "development-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_blocks
  tags = {
    Name : "development"
  }
}


resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.development-vpc.id
  cidr_block        = var.cidr_blocks[1].cidr_block
  availability_zone = "us-west-1a"
  tags = {
    Name : "subnet-1-dev"
  }
}
